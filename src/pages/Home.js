import React from 'react'
import HomeCarousel from '../components/HomeCarousel.js';
import Highlights from '../components/Highlights.js';

export default function Home(){
	return(
		<>
			<HomeCarousel />
			<Highlights />
		</>
	)
}