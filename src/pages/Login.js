import React, {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {Link, Navigate, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';
import '../css/login.css';

export default function Login() {
	const navigate = useNavigate();
	const {user,setUser} = useContext(UserContext);

	// State Hooks
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState('');

	function clearFields(){
		setEmail('');
		setPassword('');
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password])



	function loginUser(e){
		e.preventDefault();
		//console.log("Login Used");
		fetch('https://hidden-badlands-72831.herokuapp.com/users/login', {
		method: 'POST',
		headers: {'Content-Type':'application/json'},
		body: JSON.stringify({
			email: email,
			password: password
		})
	})
	.then(response => response.json())
	.then(data => {
		console.log(data);
		console.log(data.accessToken)
		if(data.accessToken !== undefined) {
			localStorage.setItem('accessToken', data.accessToken);
			setUser({accessToken: data.accessToken});

			Swal.fire({
				title: "Login",
				icon: "success",
				text: `${email} Successfully Logged In`
			})

			fetch('https://hidden-badlands-72831.herokuapp.com/users/getUserDetails', {
				headers: {
					Authorization: `Bearer ${data.accessToken}`
				}
			})
			.then(response => response.json())
			.then(result => {
				console.log(result);

				if(result.isAdmin === true) {
					localStorage.setItem('isAdmin', result.isAdmin)
					localStorage.setItem('email', result.email)
					setUser({
						email: result.email,
						isAdmin: result.isAdmin
					})
					navigate('/productlist')
				} else {
					navigate('/')
				}
			})

		} else {
			Swal.fire({
				title: 'Oops',
				icon: 'error',
				text: 'Something Went Wrong. Check your Credentials'
			})
		} //End of if
		clearFields();
	})
	}

	return(
		(user.accessToken !== null) ?
		<Navigate to="/productlist" />
		:
		<div className='d-flex justify-content-center align-items-center'>
			<Form className="mt-3 p-5 p-sm-3 rounded" onSubmit={(e) => loginUser(e)}>
			<h1>Login</h1>
			<Form.Group className='mb-3' controlId='formEmail'>
				<Form.Label>Email Address:</Form.Label>
					<Form.Control
					type="email"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)} 
					/>
			</Form.Group>

			<Form.Group className='mb-3' controlId='formPassword'>
				<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Enter Password"
						required
						value={password}
						onChange={e => setPassword(e.target.value)}
						/>
			</Form.Group>
			{isActive ?
				<Button className="mt-2" variant="primary" type="submit">Login</Button>
				:
				<Button className="mt-2" variant="primary" type="submit" disabled>Login</Button>
			}
			<p>No Account Yet? <Link as={Link} to="/register">Create New Account</Link></p>
			</Form>
			
		</div>	
	)

}