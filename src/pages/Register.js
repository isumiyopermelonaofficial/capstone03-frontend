import React, {Component, useState, useEffect} from 'react';
import Swal from 'sweetalert2';
import {Navigate, useNavigate} from 'react-router-dom';
import {Form,Button} from 'react-bootstrap';
import '../css/register.css';

export default function Register(){
	const navigate = useNavigate();

	// hooks
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	// Conditional Rendering
	const [isActive, setIsActive] = useState(false);

	useEffect(() => {
	// Validation to enable submit button when all fields are populated and both password match
	if((email !== '' && password1 !== '' && password2 !== '' && firstName !== '' && lastName !== '') && (password1 === password2)) {
		setIsActive(true)
	} else {
		setIsActive(false)
	}

	}, [email, password1, password2, firstName, lastName])

	function clear(){
		setFirstName('');
		setLastName('');
		setEmail('');
		setPassword1('');
		setPassword2('');
	}

	function registerUser(e){
		e.preventDefault()
		fetch('https://hidden-badlands-72831.herokuapp.com/users/register', {
			method:'POST',
			headers: {'Content-Type':'application/json'},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				password: password1
			})
		})
		.then(response => response.json())
		.then(result => {
			console.log(result);
			if(result === true){
				Swal.fire({
					title:"Registered",
					icon: "success",
					text: `${email} Successfully Registered`
				})
				navigate('/login')
			} else {
				Swal.fire({
					title:"Error",
					icon: "error",
					text: "Something Went Wrong, Please try again"
				})
			}
			clear();
		})
	}

	return(
		<div className='p-4 m-4 justify-content-center align-items-center'>
		<Form className="mt-3 p-4 rounded" class="form_wrapper" onSubmit={(e) => registerUser(e)}>
			<h3>Sign Up!</h3>
			<Form.Group>
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter First Name"
					required
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Last Name</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Last Name"
					required
					value={lastName}
					onChange={e => setLastName(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Email</Form.Label>
				<Form.Control 
					type="text"
					placeholder="Enter Email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
					type="password"
					placeholder="Enter Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>
			</Form.Group>
			<Form.Group>
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify Password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)} 
				/>
			</Form.Group>
			{ isActive ?
				<Button className="mt-2" variant="primary" type="submit">Register</Button>
				:
				<Button className="mt-2" variant="primary" type="submit" disabled>Register</Button>
			}
		</Form>

		</div>

	)
}