import React, {useContext,useState,useEffect} from 'react';
import UserView from '../components/UserView.js';
import AdminView from '../components/AdminView.js';
import UserContext from '../UserContext.js';

export default function ProductList(){
	const {user} = useContext(UserContext);
	const [allProducts, setAllProducts] = useState([]);

	const fetchData = () => {
        fetch(' https://hidden-badlands-72831.herokuapp.com/products/all', {
            method:'GET',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(response => response.json())
        .then(data => {
        	console.log(data);
            setAllProducts(data);
        })
    }

    useEffect(() => {
		fetchData();
	}, [])

	return(
		<>
		{(user.isAdmin === true) ?
			<AdminView productsData={allProducts} fetchData={fetchData}/>
			:
			<UserView productsData={allProducts}/>
		}
		</>
	)
}