import React, {useState} from 'react';
import './App.css';

import {Container} from 'react-bootstrap';
import {UserProvider} from './UserContext.js';
// COMPONENTS
import AppNavbar from './components/AppNavbar.js';
// PAGES
import Register from './pages/Register.js';
import Home from './pages/Home.js';
import Login from './pages/Login.js';
import ProductList from './pages/ProductList.js';
import Logout from './pages/Logout.js';
import SpecificProduct from './pages/SpecificProduct';



// For Routes
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

function App() {

  const [user,setUser] = useState({
    email: localStorage.getItem('email'),
    accessToken: localStorage.getItem('accessToken'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const unsetUser = () => {
    localStorage.clear()
  }


  return (
    <UserProvider value={{user,setUser,unsetUser}}>
    <Router>
      <AppNavbar />
      <Container>
        <Routes>
          <Route path="/" element={<Home />}/>
          <Route path="/register" element={<Register />}/>
          <Route path="/login" element={<Login />}/>
          <Route path="/productlist" element={<ProductList />}/>
          <Route path="/products/:productId" element={<SpecificProduct />}/>
          <Route path="/logout" element={<Logout />}/>
        </Routes>
      </Container>
    </Router> 
    </UserProvider>
  );
}

export default App;
