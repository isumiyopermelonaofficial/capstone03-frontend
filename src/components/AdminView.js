import React, {useState, useEffect} from 'react';
import {Table} from 'react-bootstrap';
import CreateProduct from './CreateProduct.js';
import UpdateProduct from './UpdateProduct.js';
import ArchiveProduct from './ArchiveProduct.js';
import DeleteProduct from './DeleteProduct.js';
import DeleteUser from './DeleteUser.js';

export default function AdminView(props){

	const {productsData, fetchData} = props
	const [products, setProducts] = useState([])

	useEffect(() => {
		const productArray = productsData.map(product => {
			return(
				<tr>
					<td>{product._id}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>{product.quantity}</td>
					<td className={product.isAvailable ? "text-success":"text-danger"}>
						{product.isAvailable ? "Available":"Unavailable"}
					</td>
					<td><UpdateProduct product={product._id} fetchData={fetchData}/></td>
					<td><ArchiveProduct product={product._id} isAvailable={product.isAvailable} fetchData={fetchData} /></td>
					<td><DeleteProduct product={product._id} fetchData={fetchData} /></td>
				</tr>
			)
		})
		setProducts(productArray)
	}, [productsData, fetchData])

	return (
		<>
			<div className="text-center my-4">
				<h1>Admin Dashboard</h1>
				<CreateProduct fetchData={fetchData}/>
			</div>
			<Table className='table-striped' variant='dark'>
				<thead className="bg-dark text-white">
					<th className='text-center'>ID</th>
					<th className='text-center'>Name</th>
					<th className='text-center'>Description</th>
					<th className='text-center'>Price</th>
					<th className='text-center'>Quantity</th>
					<th className='text-center'>Availability</th>
					<th className='text-center' colSpan={3}>Actions</th>
				</thead>
				<tbody>
					{products}
				</tbody>
			</Table>
		</>
	)
}