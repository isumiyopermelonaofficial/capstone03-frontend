import React from 'react';
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveProduct({product,isAvailable,fetchData}){
    
    const archiveToggle = (productId) => {
        fetch(`https://hidden-badlands-72831.herokuapp.com/products/${productId}/archive`, {
            method:'PUT',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result === true){
                Swal.fire({
                    title:'Success',
                    icon:'success',
                    text:'Product Successfully Deactivated'
                })
                fetchData();
            } else {
                Swal.fire({
                    title:'Error',
                    icon:'error',
                    text:'ERROR: Something went wrong, Please try again'
                })
                fetchData();
            }
        })
    }
    const unArchiveToggle = (productId) => {
        fetch(`https://hidden-badlands-72831.herokuapp.com/products/${productId}/activate`, {
            method:'PUT',
            headers: {
                'Content-Type':'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result === true){
                Swal.fire({
                    title:'Success',
                    icon:'success',
                    text:'Product Successfully Activated'
                })
                fetchData();
            } else {
                Swal.fire({
                    title:'Error',
                    icon:'error',
                    text:'ERROR: Something went wrong, Please try again'
                })
                fetchData();
            }
        })
    }

    return(
        <>
			{isAvailable ?
				<Button variant="danger" size="sm" onClick={() => archiveToggle(product)}>Archive</Button>
				:
				<Button variant="success" size="sm" onClick={() => unArchiveToggle(product)}>Unarchive</Button>
			}
		</>
    )
}