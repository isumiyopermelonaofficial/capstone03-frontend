import React, {useContext} from 'react';
import {Nav,Navbar} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext.js';

export default function AppNavbar(){
	const {user} = useContext(UserContext);

	return(
		<Navbar bg="dark" expand="lg" variant="dark">
			<Navbar.Brand href="#home" className="ms-2">Techly</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse>
				<Nav className="me-auto">
					<Nav.Link as={Link} to="/">Home</Nav.Link>
					{
						(user.accessToken !== null) ?
						<>
						<Nav.Link as={Link} to="/productlist">Product List</Nav.Link>
						<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
						</>
						:
						<>
						<Nav.Link as={Link} to="/login">Login</Nav.Link>
						<Nav.Link as={Link} to="/register">Register</Nav.Link>
						</>
					}
					
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	)
}
