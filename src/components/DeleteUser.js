import React from 'react'
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteUser(user, fetchData){

	const deleteUser = (userId) => {
		console.log(userId);
		fetch(`https://hidden-badlands-72831.herokuapp.com/users/${userId}`, {
			method: 'DELETE',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${localStorage.getItem('accessToken')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data === true) {

				Swal.fire({
					title: 'Success',
					icon: 'success',
					text: 'User Successfully Deleted'
				})
				fetchData()
			} else {
				Swal.fire({
					title: 'Something Went Wrong',
					icon: 'error',
					text: 'Please try again'
				})
				fetchData()
			}
		})
	}

	return(
		<Button variant="danger" size="sm" onClick={() => deleteUser(user)}>Delete</Button>
	)
}