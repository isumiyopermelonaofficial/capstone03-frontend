import React, {useState, useEffect} from 'react';
import ProductCard from './ProductCard.js';
import {Row} from 'react-bootstrap';

export default function UserView({productsData}){
	const [products, setProducts] = useState([]);

	useEffect(() => {
		const productsArray = productsData.map(product => {
			if(product.isAvailable === true) {
				return (
					<ProductCard productProp={product} key={product._id}/>
				)
			} else {
				return null;
			}
		})
		setProducts(productsArray)
	},[productsData])


	return(
		<>
			<Row>
				{products}
			</Row>
		</>
	)
}