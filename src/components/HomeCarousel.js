import React from 'react';
import {Carousel, Container} from 'react-bootstrap';

export default function HomeCarousel(){
	
	return(
		<Container>
		<Carousel>
  			<Carousel.Item interval={5000}>
			    <img
			      className="d-block w-100"
			      src="https://icms-image.slatic.net/images/ims-web/4cda03fa-a152-4a2d-a2cb-ee241aa0ed2c.jpg"
			      alt="First slide"
			    />
  			</Carousel.Item>

  			<Carousel.Item interval={5000}>
    			<img
			      className="d-block w-100"
			      src="https://lzd-img-global.slatic.net/g/icms/images/ims-web/94a8bddf-c264-43e0-b9e3-f80321d9344a.jpg_1200x1200q90.jpg_.webp"
			      alt="Second slide"
			    />
  			</Carousel.Item>

  			<Carousel.Item interval={5000}>
		    <img
		      className="d-block w-100"
		      src="https://lzd-img-global.slatic.net/g/icms/images/ims-web/8c7834f3-8c60-4d69-b997-83a368fcc081.jpg_1200x1200q90.jpg_.webp"
		      alt="Third slide"
		    />
		  	</Carousel.Item>

		</Carousel>	
		</Container>
		
	)
}