import React from 'react';
import {Row,Col,Card,Button} from 'react-bootstrap';

export default function Highlights(){
	return(
	<Row className="justify-content-center mt-4">
		<Col xs={12} md={4}>
			<Card>
  				<Card.Img variant="top" src="https://via.placeholder.com/180x180" />
  					<Card.Body>
    					<Card.Title>Featured Product</Card.Title>
    						<Card.Text>
      							Some quick example text to build on the card title and make up the bulk of
      							the card's content.
    						</Card.Text>
    						<Button variant="primary">Go somewhere</Button>
  					</Card.Body>
			</Card>
		</Col>

		<Col xs={12} md={4}>
			<Card>
				<Card.Img variant="top" src="https://via.placeholder.com/180x180" />
					<Card.Body>
    					<Card.Title>Featured Product</Card.Title>
    						<Card.Text>
      							Some quick example text to build on the card title and make up the bulk of
      							the card's content.
    						</Card.Text>
    						<Button variant="primary">Go somewhere</Button>
  					</Card.Body>
			</Card>
		</Col>

		<Col xs={12} md={4}>
			<Card>
				<Card.Img variant="top" src="https://via.placeholder.com/180x180" />
					<Card.Body>
    					<Card.Title>Featured Product</Card.Title>
    						<Card.Text>
      							Some quick example text to build on the card title and make up the bulk of
      							the card's content.
    						</Card.Text>
    						<Button variant="primary">Go somewhere</Button>
  					</Card.Body>
			</Card>
		</Col>
	</Row>

	)

}
