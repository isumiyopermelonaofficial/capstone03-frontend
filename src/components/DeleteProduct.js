import React, {useState} from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function DeleteProduct({product, fetchData}){
    const [productId, setProductId] = useState('');

    const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState('');

    const [showEdit, setShowEdit] = useState(false);

    const openEdit = (productId) => {
        fetch(`https://hidden-badlands-72831.herokuapp.com/products/${productId}`, {
            method:'GET',
            headers: {
                'Content-Type':'application/json',
                'Authorization': `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            setProductId(data._id);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setQuantity(data.quantity);
        })
        setShowEdit(true);  
    }

    const closeEdit = () => {
		setShowEdit(false);
		setName('')
		setDescription('')
		setPrice(0);
        setQuantity(0);
	}

    const deleteProduct = (e,productId) => {
        e.preventDefault();
        fetch(`https://hidden-badlands-72831.herokuapp.com/products/${productId}`, {
            method: 'DELETE',
            headers: {
                'Content-Type':'application/json',
                 Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(response => response.json())
        .then(result => {
            if(result === true){
                Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product Successfully Deleted'
				})
				closeEdit();
				fetchData();
            } else {
                Swal.fire({
					title: 'Error!',
					icon: 'error',
					text: 'Please try again'
				})
				closeEdit();
				fetchData();
            }
        })
    }

    return(
        <>
        <Button variant="danger" size="sm" onClick={() => openEdit(product)}>Delete</Button>
        <Modal show={showEdit} onHide={closeEdit}>
            <Form onSubmit={e => deleteProduct(e, productId)}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Course</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form.Group>
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Price</Form.Label>
                        <Form.Control type="number" value={price} onChange={e => setPrice(e.target.value)} required/>
                    </Form.Group>

                    <Form.Group>
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control type="number" value={quantity} onChange={e => setQuantity(e.target.value)} required/>
                    </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={closeEdit}>Close</Button>
                    <Button variant="danger" type="submit">Delete</Button>
                </Modal.Footer>
            </Form>
            
        </Modal>
        </>
    )
}