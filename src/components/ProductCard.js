import React from 'react';
import {Card,Row,Col} from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}){
    const { _id, name, description, price, quantity } = productProp

    return(
        <Col xs={12} md={3}>
            <Card border="dark" className="cardProduct mt-2">
                <Card.Img variant="top" src="https://via.placeholder.com/216x180" />
                    <Card.Body>
                        <Card.Title><h2>{name}</h2></Card.Title>
                        <Card.Text><p>Description: {description}</p></Card.Text>
                        <Card.Text><p>Price: {price}</p></Card.Text>
                        <Card.Text><p>Current Stock: {quantity}</p></Card.Text>
                        <Link className="btn btn-primary" to={`/products/${_id}`}>View Product</Link>
                    </Card.Body>
                </Card>  
        </Col>
    ) 
}

ProductCard.propTypes = {
    productProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        quantity: PropTypes.number.isRequired
    })
}